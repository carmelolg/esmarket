package it.unical.esmarket.persistence.sales.dao;

import java.util.List;

import it.unical.esmarket.domain.sales.Review;
import it.unical.esmarket.persistence.GenericDAO;

public interface ReviewDAO extends GenericDAO<Review>{
	public List<Review> getReviewByProductID(Long productID);
	public Review getReviewByProductIDAndUserID(Long productID, Long userID);
}
