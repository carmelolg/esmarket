package it.unical.esmarket.persistence.sales.dao.impl;

import it.unical.esmarket.domain.sales.SaleItem;
import it.unical.esmarket.persistence.GenericDaoImpl;
import it.unical.esmarket.persistence.sales.dao.SaleItemDAO;

public class SaleItemDAOImpl extends GenericDaoImpl<SaleItem> implements SaleItemDAO {


}
