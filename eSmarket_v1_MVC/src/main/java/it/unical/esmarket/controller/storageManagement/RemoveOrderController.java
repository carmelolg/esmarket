package it.unical.esmarket.controller.storageManagement;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import it.unical.esmarket.domain.account.ManagerShop;
import it.unical.esmarket.service.OrderService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RemoveOrderController {

	OrderService orderService;

	@Inject
	public RemoveOrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	@RequestMapping(value = "remove_order")
	public String remove(@RequestParam("id") Long id, Model model, HttpSession session) {

		orderService.removeOrder(id);
		model.addAttribute("listOfOrders", orderService.getOrdersByManager(((ManagerShop) session.getAttribute("user")).getID()));

		return "store_management/list_of_orders";
	}
}
