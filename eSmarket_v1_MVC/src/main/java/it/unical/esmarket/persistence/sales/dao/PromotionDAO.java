package it.unical.esmarket.persistence.sales.dao;

import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.persistence.GenericDAO;


public interface PromotionDAO extends GenericDAO<Promotion>{
} 
