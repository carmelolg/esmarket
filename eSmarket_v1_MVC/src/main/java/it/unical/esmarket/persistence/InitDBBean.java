package it.unical.esmarket.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.unical.esmarket.domain.account.Address;
import it.unical.esmarket.domain.account.Administrator;
import it.unical.esmarket.domain.account.Client;
import it.unical.esmarket.domain.account.ManagerShop;
import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.domain.sales.Order.OrderStatus;
import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.domain.sales.Review;
import it.unical.esmarket.domain.sales.SaleItem;
import it.unical.esmarket.domain.storageManagement.Position;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;
import it.unical.esmarket.persistence.account.dao.AccountDAO;
import it.unical.esmarket.persistence.sales.dao.OrderDAO;
import it.unical.esmarket.persistence.sales.dao.PromotionDAO;
import it.unical.esmarket.persistence.sales.dao.ReviewDAO;
import it.unical.esmarket.persistence.sales.dao.SaleItemDAO;
import it.unical.esmarket.persistence.storageManagement.dao.ProductDAO;
import it.unical.esmarket.persistence.storageManagement.dao.StoreDAO;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class InitDBBean {

	@PostConstruct
	public void fillDB() {
		DAOFactory daoFactory = DAOFactory.instance(DAOFactory.HIBERNATE);
		ProductDAO productDAO = daoFactory.getProductDAO();
		PromotionDAO promoDAO = daoFactory.getPromotionDAO();
		AccountDAO accountDAO = daoFactory.getAccountDAO();
		StoreDAO storeDAO = daoFactory.getStoreDAO();
		OrderDAO orderDAO = daoFactory.getOrderDAO();
		SaleItemDAO saleDAO = daoFactory.getSaleItemDAO();
		ReviewDAO reviewDAO = daoFactory.getReviewDAO();

		Address address = new Address();
		address.setAddress("Via Giulio Cesare 12");
		address.setCity("Cosenza");
		address.setRegion("Calabria");
		address.setState("Italia");
		address.setZipCode("00000");

		Administrator admin = new Administrator();
		admin.setAddress(address);
		admin.seteMail("admin@esmarket.com");
		admin.setPassword("aaaaa");
		admin.setUsername("admin");

		ManagerShop manager = new ManagerShop();
		manager.setAddress(address);
		manager.seteMail("manager@esmarket.com");
		manager.setPassword("aaaaa");
		manager.setUsername("manager");

		ManagerShop manager1 = new ManagerShop();
		manager1.setAddress(address);
		manager1.seteMail("manager1@esmarket.com");
		manager1.setPassword("aaaaa");
		manager1.setUsername("manager1");

		Client client = new Client();
		client.setAddress(address);
		client.seteMail("client@gmail.com");
		client.setPassword("aaaaa");
		client.setUsername("username1");

		accountDAO.save(admin);
		accountDAO.save(client);
		accountDAO.save(manager);
		accountDAO.save(manager1);

		Store mainStore = new Store();
		mainStore.setAccount(manager);
		mainStore.setName("MainStore");
		mainStore.setPosition(new Position("Via vai 32", "Reggio Calabria", "Calabria"));
		List<Product> mainStoreProducts = new ArrayList<Product>();
		mainStoreProducts
				.add(addAProduct(productDAO,
						"HP;Printers;InkJet 4 colori - Formato A4Vel max: 8,8ppm b/n, 5,2ppm colori - Ris max: 4800x1200dpi;images/products/663591.jpg;HP Envy 4500;50;89"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"CANON;Printers;InkJet a 4 colori - Formato A4Vel max: 8ppm b/n, 4pp, colori - Ris max: 4800x600dpi;images/products/663912.jpg;CANON Pixma MG2550 ;80;49"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"NIKON;Cameras;Fotocamera Reflex digitale - Sensore da 24 Megapixel LCD da 3\" - Flash TTL - Slot SD/SDHC - Uscita HDMI;images/products/642311.jpg; NIKON   D3200 Kit 18-55 VR ;60;469"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"CANON;Cameras;Fotocamera Reflex digitale - Sensore CMOS da 18 Megapixel LCD da 3\" Orientabile - Filmati Full HD;images/products/622990.jpg; CANON   EOS 600D Kit 18-55 IS ;40;479"));
		mainStoreProducts.add(addAProduct(productDAO,
				"SONY;MP3 readers;Lettore MP3 - Memoria interna: 4GB;images/products/639900.jpg; SONY   NWZ-B173 Black ;500;49"));
		mainStoreProducts.add(addAProduct(productDAO,
				"SHARP;TVs;SMART TV LED 3D 80\" Full HD - Risoluzione: 1920x1080;images/products/662350.jpg; SHARP   LC-80LE657E ;20;4749"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"LG;Smartphones;Quadriband - 3G - LTE - Wi-Fi Fotocamera da 13 Megapixel - Memoria interna 16GB Android 4.1.2 ;images/products/659752.jpg; LG   Optimus G Pro E986 Black ;40;599"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"SAMSUNG;Smartphones;Quadriband - 3G - LTE - Wi-Fi Fotocamera da 13 Megapixel - Processore Quad Core 2.3 GHz Android 4.3 Jelly Bean - Memoria 32GB - NFC Display Super Amoled 5,7\" Touchscreen - GPS integrato Distribuito da Samsung Italia;images/products/662820.jpg;SAMSUNG Galaxy Note 3;10;629"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"TOSHIBA;Notebooks;Intel Core i3 3120M (2.50GHz, 3MB L3) HD 500GB - RAM 4GB - Display 15,6\" LED TruBriteŽ HD Wi-Fi 802.11b/g/n - Onkyo Speakers - Windows 8 64-bit Scheda video NVIDIA GeForce GT 710M 1G dedicata;images/products/668067.jpg; TOSHIBA   Satellite L50-A-143 ;25;479"));
		mainStoreProducts
				.add(addAProduct(
						productDAO,
						"HP;Notebooks;Processore AMD E Series E1-2500 (1.40, 2MB L2) HD 500GB - RAM 4GB - Display 15,6\" wide Wi-Fi 802.11b/g/n - Microsoft Windows 8 64-bit Scheda grafica AMD Radeon HD 8240;images/products/667855.jpg; HP   Pavilion 15-N020el ;10;349"));
		mainStore.setProducts(mainStoreProducts);
		storeDAO.save(mainStore);

		Store subStore = new Store();
		subStore.setAccount(manager1);
		subStore.setName("STORE2");
		subStore.setPosition(new Position("Via Giulio Cesare 3", "Cosenza", "Calabria"));
		List<Product> subStoreProducts = new ArrayList<Product>();
		subStoreProducts
				.add(addAProduct(productDAO,
						"HP;Printers;InkJet 4 colori - Formato A4Vel max: 8,8ppm b/n, 5,2ppm colori - Ris max: 4800x1200dpi;images/products/663591.jpg;HP Envy 4500;5;89"));
		subStoreProducts
				.add(addAProduct(
						productDAO,
						"CANON;Printers;InkJet a 4 colori - Formato A4Vel max: 8ppm b/n, 4pp, colori - Ris max: 4800x600dpi;images/products/663912.jpg;CANON Pixma MG2550 ;20;49"));
		subStoreProducts
				.add(addAProduct(
						productDAO,
						"NIKON;Cameras;Fotocamera Reflex digitale - Sensore da 24 Megapixel LCD da 3\" - Flash TTL - Slot SD/SDHC - Uscita HDMI;images/products/642311.jpg; NIKON   D3200 Kit 18-55 VR ;12;469"));
		subStoreProducts
				.add(addAProduct(
						productDAO,
						"CANON;Cameras;Fotocamera Reflex digitale - Sensore CMOS da 18 Megapixel LCD da 3\" Orientabile - Filmati Full HD;images/products/622990.jpg; CANON   EOS 600D Kit 18-55 IS ;10;479"));
		subStore.setProducts(subStoreProducts);
		storeDAO.save(subStore);

		Order order = new Order();
		order.setDestinationAddress(client.getAddress());
		order.setOrderManager(manager1);
		order.setOrderOwner(client);
		order.setPosition(new Position(address.getAddress(), address.getCity(), address.getRegion()));
		order.setRequestDate(new Date(System.currentTimeMillis()));
		order.setStatus(OrderStatus.COMPLETED);
		order.setTotalPrice();
		SaleItem item1 = new SaleItem();
		item1.setProduct(subStoreProducts.get(0));
		item1.setQuantity(1);
		SaleItem item2 = new SaleItem();
		item2.setProduct(subStoreProducts.get(1));
		order.addItem(item1);
		order.addItem(item2);
		orderDAO.validAndSaveOrder(order);

		Review review = new Review();
		review.setAuthor(client);
		review.setReviewedProduct(subStoreProducts.get(0));
		review.setReviewText("Questa stampante spacca!!!");
		reviewDAO.save(review);

		Promotion promotion = new Promotion();
		promotion.setDiscount(10);
		promotion.setStartPromo(new Date(System.currentTimeMillis()));
		promotion.setEndPromo(new Date(System.currentTimeMillis()));
		promoDAO.save(promotion);

		List<Product> productsWithDiscount = productDAO.findProductsByCategory("Printers");
		for (Product product : productsWithDiscount) {
			product.setPromotion(promotion);
			productDAO.update(product);
		}

	}

	public Product addAProduct(ProductDAO productDAO, String params) {
		String[] paramList = params.split(";");
		Product product = new Product();
		product.setBrand(paramList[0]);
		product.setCategory(paramList[1]);
		product.setDescription(paramList[2]);
		product.setForSale(true);
		product.setImage(paramList[3]);
		product.setProductDate(new Date(System.currentTimeMillis()));
		product.setName(paramList[4]);
		product.setQuantity(Integer.valueOf(paramList[5]));
		product.setUnitPrice(Float.valueOf(paramList[6]));
		productDAO.save(product);
		return product;
	}
}
