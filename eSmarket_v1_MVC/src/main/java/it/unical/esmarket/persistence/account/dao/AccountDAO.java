package it.unical.esmarket.persistence.account.dao;

import java.util.List;

import it.unical.esmarket.domain.account.Account;
import it.unical.esmarket.persistence.GenericDAO;

public interface AccountDAO extends GenericDAO<Account>{	

	public boolean findMail(String mail);
	public Account findAccountByAuthenticationINFO(String mail, String password);
	public List<Account> getManagers();
	public Account getAdiministrator();
}
