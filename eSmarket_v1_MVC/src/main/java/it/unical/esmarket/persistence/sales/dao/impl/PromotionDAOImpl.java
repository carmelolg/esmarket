package it.unical.esmarket.persistence.sales.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.persistence.GenericDaoImpl;
import it.unical.esmarket.persistence.sales.dao.PromotionDAO;
import it.unical.esmarket.util.HibernateUtil;

public class PromotionDAOImpl extends GenericDaoImpl<Promotion> implements PromotionDAO {

	@Override
	public void remove(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Query query = session.createQuery("from Product p where p.promotion.ID=:ID");
			query.setParameter("ID", id);
			List<Product> products = query.list();
			for (Product product : products) {
				product.setPromotion(null);
				session.update(product);
			}
			Promotion promotion = (Promotion) session.get(Promotion.class, id);
			session.delete(promotion);
			transaction.commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
	}

}
