package it.unical.esmarket.persistence.storageManagement.dao;

import java.util.List;

import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;
import it.unical.esmarket.persistence.GenericDAO;

public interface StoreDAO extends GenericDAO<Store>{

	public Store getByName(String name);
	public Store getStoreByAccount(Long idAccount);
	public List<Store> getStoresContainingProuducts(List<String> productNames);
	public Store getLazyInitializedStore(Long storeID);
}
