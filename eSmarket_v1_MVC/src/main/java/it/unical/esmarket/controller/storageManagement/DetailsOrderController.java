package it.unical.esmarket.controller.storageManagement;

import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.domain.storageManagement.Store;
import it.unical.esmarket.service.OrderService;
import it.unical.esmarket.service.StoreService;

import javax.inject.Inject;

import org.omg.CORBA.ORB;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DetailsOrderController {

	OrderService orderService;
	StoreService storeService;
	
	@Inject
	public DetailsOrderController(OrderService os, StoreService ss) {
		this.orderService = os;
		this.storeService = ss;
	}
	
	@RequestMapping(value = "details_order")
	public String details_prod(@RequestParam("idOrd")Long idOrder, Model model) {
		
		Order order = orderService.getOrderByID(idOrder,false);
		model.addAttribute("order", order);
		Store store = storeService.getStoreByAccount(order.getOrderManager().getID());
		model.addAttribute("storeID", store.getID());
		return "store_management/details_order";
	}
}
