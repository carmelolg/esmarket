package it.unical.esmarket.persistence.storageManagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;
import it.unical.esmarket.persistence.GenericDaoImpl;
import it.unical.esmarket.persistence.storageManagement.dao.StoreDAO;
import it.unical.esmarket.util.HibernateUtil;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class StoreDAOImpl extends GenericDaoImpl<Store> implements StoreDAO {

	@Override
	public Store getByName(String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Store object;
		try {
			object = (Store) session.createQuery("from Store where STORE_NAME = :name").setParameter("name", name).list().get(0);
			return object;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		//session.flush();

		transaction.commit();
		session.close();
		return null;
	}

	@Override
	public List<Store> getStoresContainingProuducts(List<String> productNames) {
		List<Store> allStores = getAll();
		List<Store> result = new ArrayList<Store>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try {
			for (Store store : allStores) {
				Criteria criteria = session.createCriteria(Store.class).add(Restrictions.eq("ID", store.getID())).createAlias("products", "p")
						.setProjection(Projections.property("p.name"));
				List<String> pn = criteria.list();
				if (pn.containsAll(productNames))
					result.add(store);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} 
		//session.flush();
		transaction.commit();
		session.close();

		return result;

	}

	@Override
	public Store getStoreByAccount(Long idAccount) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Store object;
		try {
			object = (Store) session.createQuery("select distinct s from Store s join s.account a where a.id = :idAccount")
					.setParameter("idAccount", idAccount).uniqueResult();
			return object;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		//session.flush();
		transaction.commit();
		session.close();
		return null;
	}

	@Override
	public Store getLazyInitializedStore(Long storeID) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Store store = null;
		try {
			transaction = session.beginTransaction();
			store = (Store) session.get(Store.class, storeID);
			Hibernate.initialize(store.getProducts());
			//session.flush();
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return store;
	}

}
