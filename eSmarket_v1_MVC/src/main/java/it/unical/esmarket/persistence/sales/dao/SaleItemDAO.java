package it.unical.esmarket.persistence.sales.dao;

import it.unical.esmarket.domain.sales.SaleItem;
import it.unical.esmarket.persistence.GenericDAO;

import org.hibernate.id.CompositeNestedGeneratedValueGenerator.GenerationPlan;

public interface SaleItemDAO extends GenericDAO<SaleItem>{

}
