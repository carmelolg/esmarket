//package it.unical.esmarket.client.test;
//
//import it.unical.esmarket.domain.account.Account;
//import it.unical.esmarket.domain.account.Address;
//import it.unical.esmarket.domain.account.Client;
//import it.unical.esmarket.domain.account.ManagerShop;
//import it.unical.esmarket.domain.sales.Order;
//import it.unical.esmarket.domain.sales.Promotion;
//import it.unical.esmarket.domain.storageManagement.Product;
//import it.unical.esmarket.persistence.account.dao.AccountDAO;
//import it.unical.esmarket.persistence.account.dao.impl.AccountDaoImpl;
//import it.unical.esmarket.persistence.sales.dao.OrderDAO;
//import it.unical.esmarket.persistence.sales.dao.PromotionDAO;
//import it.unical.esmarket.persistence.sales.dao.impl.OrderDAOImpl;
//import it.unical.esmarket.persistence.sales.dao.impl.PromotionDAOImpl;
//import it.unical.esmarket.persistence.storageManagement.dao.ProductDAO;
//import it.unical.esmarket.persistence.storageManagement.dao.impl.ProductDAOImpl;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//public class Main {
//
//	public static void main(String[] args) {
//
//		AccountDAO accountDAO = new AccountDaoImpl();
//
//		Account accountClient = new Client();
//		accountClient.setUsername("melo");
//		accountClient.setPassword("lacoscia");
//		Address address = new Address("via Roma", "Porto Salvo, Vibo Valentia",
//				"Calabria", "Italia", 89900);
//		accountClient.setAddress(address);
//		accountClient.seteMail("melo@melo.com");
//		accountClient.setManager(false);
//		accountDAO.addAccount(accountClient);
//
//		// System.out.println(accountDAO.getAccounts().size());
//
//		Account accountManager = new ManagerShop();
//		accountManager.setUsername("maurizia");
//		accountManager.setPassword("mocciolo");
//		Address addressManager = new Address("via benestare",
//				"Bovalino, Reggio Calabria", "Calabria", "Italia", 89034);
//		accountManager.setAddress(addressManager);
//		accountManager.seteMail("mauriz@aaaex.com");
//		accountManager.setManager(true);
//		accountDAO.addAccount(accountManager);
//
//		Promotion pro = new Promotion();
//		pro.setCategory("Computers");
//		pro.setDiscount(12);
//		pro.setEndPromo(new Date(3600));
//		pro.setStartPromo(new Date(1200));
//		PromotionDAO promotionDAO = new PromotionDAOImpl();
//		// promotionDAO.addPromotion(pro);
//
//		Product p = new Product();
//		p.setName("Toshiba Satellite Pro");
//		p.setQuantity(1);
//		p.setUnitPrice(1290);
//		p.setCategory("Computers");
//		p.setPromotion(pro);
//		ProductDAO productDAO = new ProductDAOImpl();
//		((ProductDAOImpl)productDAO).save(p);
//
//		Order order = new Order();
//		order.setStatus(Order.OrderStatus.SUBMITTED);
//		order.setRequestOrder(new Date(3600));
//		order.setOrderOwner((Client) accountClient);
//		order.setOrderManager((ManagerShop) accountManager);
//		List<Product> products = new ArrayList<Product>();
//		products.add(p);
//		order.setProducts(products);
//		order.setTotalPrice();
//
//		OrderDAO orderDAO = new OrderDAOImpl();
//		Long idOrder = orderDAO.addOrder(order);
//		System.err.println(products.size() + " " + order.getProducts().size());
//		orderDAO.getOrder(idOrder);
//
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}
//}