package it.unical.esmarket.util;

import it.unical.esmarket.domain.account.Account;
import it.unical.esmarket.domain.account.Administrator;
import it.unical.esmarket.domain.account.Client;
import it.unical.esmarket.domain.account.ManagerShop;
import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.domain.sales.Review;
import it.unical.esmarket.domain.sales.SaleItem;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new Configuration().configure("resource/hibernate.cfg.xml").addPackage("it.unical.esmarket.domain.account")
					.addPackage("it.unical.esmarket.domain.sales").addPackage("it.unical.esmarket.domain.storageManagement")
					.addPackage("it.unical.esmarket.service").addAnnotatedClass(Account.class).addAnnotatedClass(Client.class)
					.addAnnotatedClass(ManagerShop.class).addAnnotatedClass(Order.class).addAnnotatedClass(Product.class)
					.addAnnotatedClass(Store.class).addAnnotatedClass(Promotion.class).addAnnotatedClass(SaleItem.class)
					.addAnnotatedClass(Administrator.class).addAnnotatedClass(Review.class).buildSessionFactory();

		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
