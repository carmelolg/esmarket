package it.unical.esmarket.service;

import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.persistence.DAOFactory;
import it.unical.esmarket.persistence.sales.dao.PromotionDAO;
import it.unical.esmarket.persistence.storageManagement.dao.ProductDAO;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class PromotionService {

	private PromotionDAO promotionDAO;
	private ProductDAO productDAO;

	public PromotionService() {
		DAOFactory daoFactory = DAOFactory.instance(DAOFactory.HIBERNATE);
		promotionDAO = daoFactory.getPromotionDAO();
		productDAO = daoFactory.getProductDAO();
	}

	public List<Promotion> getAllPromotions(){
		return promotionDAO.getAll();
	}
	
	public void removePromotion(Promotion promotion) {
		promotionDAO.remove(promotion.getID());
	}

	public void savePromotion(Promotion promotion) {
		promotionDAO.save(promotion);
	}

	public Promotion getPromotionByID(Long idPromo) {
		return promotionDAO.getById(idPromo);
	}

	public String getCategoryByPromotion(Promotion promotion) {
		List<Product> products = productDAO.getAll();
		for (Product p : products) {
			if (p.getPromotion() != null) {
				if (p.getPromotion().getID() == promotion.getID()) {
					return p.getCategory();
				}
			}
		}
		return "all";
	}

}
