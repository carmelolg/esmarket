package it.unical.esmarket.controller.storageManagement;

import it.unical.esmarket.service.AuthenticationService;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ManagerListController {

	private AuthenticationService authenticationService;

	@Inject
	public ManagerListController(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	@RequestMapping(value = "list_of_manager")
	public String list_of_manager(Model model) {
		model.addAttribute("listOfAccount", authenticationService.getManagers());
		return "store_management/list_of_manager";
	}

}
