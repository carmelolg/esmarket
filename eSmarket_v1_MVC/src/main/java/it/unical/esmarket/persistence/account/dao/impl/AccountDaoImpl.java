package it.unical.esmarket.persistence.account.dao.impl;

import it.unical.esmarket.domain.account.Account;
import it.unical.esmarket.domain.account.Administrator;
import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.domain.sales.Review;
import it.unical.esmarket.persistence.GenericDaoImpl;
import it.unical.esmarket.persistence.account.dao.AccountDAO;
import it.unical.esmarket.util.HibernateUtil;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

public class AccountDaoImpl extends GenericDaoImpl<Account> implements AccountDAO {

	@Override
	public void remove(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Query query = session.createQuery("from Order o where o.orderOwner.id=:ID");
			query.setParameter("ID", id);
			List<Order> orders = query.list();
			for (Order order : orders) {
				order.setOrderOwner(null);
				session.update(order);
			}

			query = session.createQuery("from Order o where o.orderManager.id=:ID");
			query.setParameter("ID", id);
			orders = query.list();
			for (Order order : orders) {
				order.setOrderManager(null);
				session.update(order);
			}

			query = session.createQuery("from Review r where r.author.id=:ID");
			query.setParameter("ID", id);
			List<Review> reviews = query.list();
			for (Review review : reviews) {
				review.setAuthor(null);
				session.update(review);
			}

			Account account = (Account) session.get(Account.class, id);
			session.delete(account);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
	}

	@Override
	public boolean findMail(String mail) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Account.class).add(Restrictions.eq("eMail", mail));
		return findByCriteria(criteria).size() > 0;
	}

	@Override
	public Account findAccountByAuthenticationINFO(String mail, String password) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Account.class).add(Restrictions.eq("eMail", mail))
				.add(Restrictions.eq("password", password));
		List<Account> accounts = findByCriteria(criteria);
		if (accounts.size() > 0)
			return findByCriteria(criteria).get(0);
		else
			return null;
	}

	@Override
	public List<Account> getManagers() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		List<Account> accounts = null;
		try {
			Query query = session.createQuery("from Account where type_of_user = :manager").setParameter("manager", "Manager");
			accounts = query.list();

			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return accounts;
	}

	@Override
	public Account getAdiministrator() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Administrator.class);
		return findByCriteria(criteria).get(0);
	}

}
