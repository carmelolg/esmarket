package it.unical.esmarket.test.service;

import it.unical.esmarket.domain.account.Account;
import it.unical.esmarket.domain.account.Address;
import it.unical.esmarket.domain.account.Administrator;
import it.unical.esmarket.domain.account.Client;
import it.unical.esmarket.domain.account.ManagerShop;
import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.domain.sales.Order.OrderStatus;
import it.unical.esmarket.domain.sales.Promotion;
import it.unical.esmarket.domain.sales.Review;
import it.unical.esmarket.domain.sales.SaleItem;
import it.unical.esmarket.domain.storageManagement.Position;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;
import it.unical.esmarket.persistence.DAOFactory;
import it.unical.esmarket.persistence.account.dao.AccountDAO;
import it.unical.esmarket.persistence.sales.dao.OrderDAO;
import it.unical.esmarket.persistence.sales.dao.PromotionDAO;
import it.unical.esmarket.persistence.sales.dao.ReviewDAO;
import it.unical.esmarket.persistence.sales.dao.SaleItemDAO;
import it.unical.esmarket.persistence.storageManagement.dao.ProductDAO;
import it.unical.esmarket.persistence.storageManagement.dao.StoreDAO;
import it.unical.esmarket.service.AuthenticationService;
import it.unical.esmarket.service.OrderService;
import it.unical.esmarket.service.StoreService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.LazyInitializationException;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServicesTest {

	public static DAOFactory daoFactory;
	public static OrderService orderService;
	public static StoreService storeService;
	public static AuthenticationService authenticationService;

	@BeforeClass
	public static void fillDB() {
		daoFactory = DAOFactory.instance(DAOFactory.HIBERNATE);
		orderService = new OrderService();
		storeService = new StoreService();
		authenticationService = new AuthenticationService();
		fillDataBase();
	}

	@Test
	public void buyMoreProductsThenAvailables() {
		Order order = new Order();
		orderService.addSaleItemToOrder(order, daoFactory.getProductDAO().getAll().get(0), 1000);

		ManagerShop manager = (ManagerShop) storeService.getStoresContainingOrderProducts(order).get(0).getAccount();
		order.setOrderManager(manager);
		Client orderOwner = (Client) authenticationService.loginAccount("a@a.a", "aaaaa");
		order.setOrderOwner(orderOwner);

		Assert.assertTrue(!orderService.saveOrder(order));
	}

	@Test
	public void alreadyrRegistredMailRegistration() {
		Client newClient = new Client();
		newClient.seteMail("a@a.a");
		newClient.setPassword("aaaaa");
		newClient.setUsername("newUser");
		Assert.assertTrue(!authenticationService.registerUser(newClient));
	}

	@Test
	public void wrongLoginCredentials() {
		Account account = authenticationService.loginAccount("wrongMail", "wrongPassword");
		Assert.assertNull(account);
	}

	@Test(expected = LazyInitializationException.class)
	public void lazyLoadStore() {
		ManagerShop manager = (ManagerShop) authenticationService.loginAccount("m@a.a", "aaaaa");
		Store store = storeService.getStoreByAccount(manager.getID());
		Store lazyLoadStore = storeService.getById(store.getID(), true);
		lazyLoadStore.getProducts().get(0);
	}

	@Test(expected = LazyInitializationException.class)
	public void lazyLoadOrder() {
		Client client = (Client) authenticationService.loginAccount("a@a.a", "aaaaa");
		Order order = orderService.getOrdersByClient(client.getID()).get(0);

		Order lazyInitializedOrder = orderService.getOrderByID(order.getID(), true);
		lazyInitializedOrder.getItems().get(0);
	}

	public static void fillDataBase() {
		ProductDAO productDAO = daoFactory.getProductDAO();
		PromotionDAO promoDAO = daoFactory.getPromotionDAO();
		AccountDAO accountDAO = daoFactory.getAccountDAO();
		StoreDAO storeDAO = daoFactory.getStoreDAO();
		OrderDAO orderDAO = daoFactory.getOrderDAO();
		SaleItemDAO saleDAO = daoFactory.getSaleItemDAO();
		ReviewDAO reviewDAO = daoFactory.getReviewDAO();

		Client client = new Client();
		Address address = new Address();
		address.setAddress("address1");
		address.setCity("city1");
		address.setRegion("region1");
		address.setState("state1");
		address.setZipCode("00000");

		client.setAddress(address);
		client.seteMail("a@a.a");
		client.setPassword("aaaaa");
		client.setUsername("username1");

		accountDAO.save(client);

		ManagerShop manager = new ManagerShop();

		manager.setAddress(address);
		manager.seteMail("m@a.a");
		manager.setPassword("aaaaa");
		manager.setUsername("manager");

		accountDAO.save(manager);

		Administrator admin = new Administrator();

		admin.setAddress(address);
		admin.seteMail("q@a.a");
		admin.setPassword("aaaaa");
		admin.setUsername("admin");

		accountDAO.save(admin);

		Promotion promotion = new Promotion();
		promotion.setDiscount(10);
		promotion.setStartPromo(new Date(System.currentTimeMillis()));
		promotion.setEndPromo(new Date(System.currentTimeMillis()));
		promoDAO.save(promotion);

		Store forSaleStore = new Store();
		forSaleStore.setAccount(manager);
		forSaleStore.setName("adminStore");
		forSaleStore.setPosition(new Position("adminAddress", "cityAddress", "regionAddress"));

		for (int i = 0; i < 50; i++) {
			Product product = new Product();
			product.setBrand("brand" + i);
			product.setCategory("category" + i / 20);
			product.setDescription("description" + i);
			product.setForSale(true);
			product.setImage("images/products/product.jpg");
			product.setName("name" + i);
			product.setPromotion(promotion);
			product.setQuantity(5);
			product.setUnitPrice(i + 10);
			product.setProductDate(new Date(System.currentTimeMillis()));
			product = productDAO.save(product);
			forSaleStore.addProductToTheStore(product);

			Review review = new Review();
			review.setAuthor(client);
			review.setReviewedProduct(product);
			review.setReviewText("Questa lumix � una figata");
			reviewDAO.save(review);
		}

		storeDAO.save(forSaleStore);

		Order order = new Order();
		order.setOrderManager(manager);
		order.setOrderOwner(client);
		order.setPosition(new Position("add", "city", "region"));
		List<SaleItem> saleItems = new ArrayList<SaleItem>();
		SaleItem saleItem = new SaleItem();
		List<Product> products2 = productDAO.getAll();
		saleItem.setProduct(products2.get(0));
		saleDAO.save(saleItem);
		saleItems.add(saleItem);
		order.setItems(saleItems);
		order.setStatus(OrderStatus.COMPLETED);
		order.setTotalPrice();
		order.setDestinationAddress(address);
		orderDAO.save(order);
	}

}
