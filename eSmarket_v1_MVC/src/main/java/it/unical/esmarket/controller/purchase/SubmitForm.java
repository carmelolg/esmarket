package it.unical.esmarket.controller.purchase;

import javax.validation.Valid;

import it.unical.esmarket.domain.account.Address;

import org.springframework.stereotype.Component;

@Component
public class SubmitForm {

	private String deliveryMode;
	private String addressSelected;
	@Valid
	private Address address;
	private Long storeID;

	public SubmitForm() {
		deliveryMode = new String();
		addressSelected = new String();
		address = new Address();
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	public Long getStoreID() {
		return storeID;
	}

	public void setStoreID(Long storeID) {
		this.storeID = storeID;
	}

	public String getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public String getAddressSelected() {
		return addressSelected;
	}

	public void setAddressSelected(String addressSelected) {
		this.addressSelected = addressSelected;
	}
}