package it.unical.esmarket.controller;

import it.unical.esmarket.controller.purchase.SearchForm;
import it.unical.esmarket.domain.sales.Order;
import it.unical.esmarket.service.ProductsService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class RequestsInterceptor implements HandlerInterceptor {

	private ProductsService productsService;

	@Inject
	public RequestsInterceptor(ProductsService productsService) {
		this.productsService = productsService;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		initializeSession(request.getSession());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		if (modelAndView != null) {
			modelAndView.addObject("categories", productsService.getProductsCategories());
			modelAndView.addObject("brands", productsService.getAllBrands());
			modelAndView.addObject("searchForm", request.getSession().getAttribute("searchForm"));
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		// TODO Auto-generated method stub

	}

	public void initializeSession(HttpSession session) {
		if (session.getAttribute("pageNum") == null) {
			session.setAttribute("pageNum", 1);
			session.setAttribute("orderOption", 0);
			session.setAttribute("searchForm", new SearchForm());
			session.setAttribute("order", new Order());
		}

	}

}
