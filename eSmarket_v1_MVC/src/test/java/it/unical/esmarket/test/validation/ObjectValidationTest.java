package it.unical.esmarket.test.validation;

import it.unical.esmarket.domain.account.Address;
import it.unical.esmarket.domain.account.Client;
import it.unical.esmarket.domain.storageManagement.Product;
import it.unical.esmarket.domain.storageManagement.Store;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

public class ObjectValidationTest {

	private static Validator validator;

	@BeforeClass
	public static void initializeDaoFactories() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();

	}

	@Test
	public void createAccountWithInvalidValues() {
		Client client = new Client();
		client.setUsername("INVALID_USERNAME");
		client.setAddress(new Address("address", "city", "region", "state", "INVALID_ZIP_CODE"));
		client.seteMail("INVALID_MAIL_ADDRESS");
		client.setPassword("IP");
		Set<ConstraintViolation<Client>> violations = validator.validate(client);
		Assert.assertTrue(violations.size() == 4);
	}

	@Test
	public void createStoreWithInvalidValues() {
		Store store = new Store();
		store.setName("INVALID_NAME");
		Set<ConstraintViolation<Store>> violations = validator.validate(store);
		Assert.assertTrue(violations.size() == 1);
	}

	@Test
	public void createProductWithInvalidValues() {
		Product product = new Product();
		product.setBrand("INVALID_BRAND");
		product.setCategory("INVALID_CATEGORY");
		product.setName("INVALID_NAME");
		Set<ConstraintViolation<Product>> violations = validator.validate(product);
		Assert.assertTrue(violations.size() == 3);
	}

	public void createAddressWithInvalidInformation() {
		Address address = new Address();
		address.setZipCode("INVALID_ZIP_CODE");
		Set<ConstraintViolation<Address>> violations = validator.validate(address);
		Assert.assertTrue(violations.size() == 1);
	}

}
